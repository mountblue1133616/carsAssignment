const carsBefore2000=(inventory)=>{
    if(!inventory){
        return [];
    }
    let olderCars=[];
    for (let index=0;index<inventory.length;index++){
        if(inventory[index].car_year<2000){
            olderCars.push(inventory[index])
        }
    }
    return olderCars;
}

module.exports=carsBefore2000;