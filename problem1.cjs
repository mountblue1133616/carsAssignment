const findCarByID=(inventory,id)=>{
    if(!inventory || id==='undefined' || typeof(id)!=='number' || inventory.length===0 ||
    typeof(inventory)!=='object' ||
    typeof(inventory[0])!=='object'
)
    {
        return[];
    }
    for(let index=0;index<inventory.length;index++){
        if (inventory[index].id===id)
        return inventory[index];
    }
}

module.exports=findCarByID;