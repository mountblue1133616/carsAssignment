const BMWAndAudi=(inventory)=>{
    if(!inventory){
        return [];
    }
    let cars=[];
    for (let index=0;index<inventory.length;index++){
        if(inventory[index].car_make=='Audi' || inventory[index].car_make=='BMW'){
            cars.push(inventory[index])
        }
    }
    return cars;
}

module.exports=BMWAndAudi;