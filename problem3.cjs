const sortByAlphabet=(inventory)=>{
  if(!inventory){
        return [];
    }
  const carModels=[]
  for (let index=0;index<inventory.length;index++){
    carModels.push(inventory[index].car_model)
  }
  carModels.sort();
  return carModels;
}

module.exports=sortByAlphabet;
